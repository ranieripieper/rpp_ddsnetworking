# DDSNetworking

[![CI Status](http://img.shields.io/travis/Gilson Gil/DDSNetworking.svg?style=flat)](https://travis-ci.org/Gilson Gil/DDSNetworking)
[![Version](https://img.shields.io/cocoapods/v/DDSNetworking.svg?style=flat)](http://cocoapods.org/pods/DDSNetworking)
[![License](https://img.shields.io/cocoapods/l/DDSNetworking.svg?style=flat)](http://cocoapods.org/pods/DDSNetworking)
[![Platform](https://img.shields.io/cocoapods/p/DDSNetworking.svg?style=flat)](http://cocoapods.org/pods/DDSNetworking)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DDSNetworking is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DDSNetworking"
```

## Author

Gilson Gil, gilson.gil@doisdoissete.com

## License

DDSNetworking is available under the MIT license. See the LICENSE file for more info.
